package de.wumpitz.howlwebapi;

import com.google.gson.internal.LinkedTreeMap;
import dalvik.annotation.TestTargetClass;
import de.wumpitz.howlwebapi.models.Binding;
import de.wumpitz.howlwebapi.models.Device;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;
import org.robolectric.RobolectricTestRunner;

import java.lang.System;
import java.util.Iterator;
import java.util.Map;

/**
 * Just a quick and dirty test implementation for development.
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = "src/main/AndroidManifest.xml", emulateSdk = 18)
public class DummyTestScript {

    @Test
    public void shouldGetBinding() {
        HowlService howlService = new HowlApi().getService();
        for (Binding binding : howlService.getBindings()) {
            Binding newBinding = howlService.getBinding(binding.code);
            assert newBinding.equals(binding);
        }
    }

    @Test
    public void shouldGetDevices() {
        HowlService howlService = new HowlApi().getService();
        Binding binding = howlService.getBindings().get(0);
        for (Device device : howlService.getDevices(binding.code)) {
            LinkedTreeMap ltm = (LinkedTreeMap) device.fields;
            System.out.println(ltm.get("address"));
            for ( Iterator<Map.Entry> iterator = ltm.entrySet().iterator(); iterator.hasNext(); )
                System.out.println( iterator.next() );
        }
    }

    @Test
    public void shouldGetDevice() {
        HowlService howlService = new HowlApi().getService();
        Binding binding = howlService.getBindings().get(0);
        for (Device device : howlService.getDevices(binding.code)) {
            Device newDevice = howlService.getDevice(device.getId());
            assert newDevice.equals(device);
        }
    }
}
