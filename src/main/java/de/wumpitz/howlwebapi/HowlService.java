package de.wumpitz.howlwebapi;

import de.wumpitz.howlwebapi.models.Binding;
import de.wumpitz.howlwebapi.models.Device;
import retrofit.http.GET;
import retrofit.http.Path;

import java.util.List;

public interface HowlService {

    @GET("/bindings/")
    List<Binding> getBindings();

    @GET("/bindings/{code}")
    Binding getBinding(@Path("code") String code);

    @GET("/bindings/{code}/devices/")
    List<Device> getDevices(@Path("code") String code);

    @GET("/devices/{id}")
    Device getDevice(@Path("id") String id);
}
